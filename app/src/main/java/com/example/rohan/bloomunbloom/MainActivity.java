package com.example.rohan.bloomunbloom;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {
    public TextView t1;
    Response response;
    String response1;
    public EditText et1;
    public Button bt1;
    public ImageView imageView;
    public Bitmap bitmap;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        t1=(TextView)findViewById(R.id.tf1);
        imageView=(ImageView)findViewById(R.id.iv1);
        et1=(EditText)findViewById(R.id.et1);
        bt1=(Button)findViewById(R.id.bt1);
        bitmap=null;
        response1=new String();
        bt1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getJSON(et1.getText().toString());
//                startActivity(new Intent(MainActivity.this,UserHome.class));
            }
        });



    }

    private void getJSON(final String s) {

        class GetJSON extends AsyncTask<Void, Void, String> {

            @Override
            protected void onPreExecute()
            {
                Toast.makeText(getApplicationContext(), "Pre", Toast.LENGTH_SHORT).show();
                super.onPreExecute();
            }


            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
//                Toast.makeText(getApplicationContext(), s, Toast.LENGTH_SHORT).show();
//                Toast.makeText(getApplicationContext(),"Toast is"+ s, Toast.LENGTH_SHORT).show();
                response1=s;
                loadInTF(s);
            }

            @Override
            protected String doInBackground(Void... voids)
            {
                try
                {
                    //sample link "https://www.almanac.com/sites/default/files/styles/primary_image_in_article/public/image_nodes/sunflower-1627193_1920.jpg"
                    //                    Toast.makeText(getApplicationContext(), "Back", Toast.LENGTH_SHORT).show();
//                          Toast.makeText(getApplicationContext(), "called", Toast.LENGTH_SHORT).show();
                    OkHttpClient client = new OkHttpClient();
                    bitmap = BitmapFactory.decodeStream((InputStream)new URL(s).getContent());
                    RequestBody formBody = new FormBody.Builder()
                            .add("urls", s).build();

                    Request request = new Request.Builder()
                            .url("https://app.nanonets.com/api/v2/ObjectDetection/Model/18edf583-4d3d-4924-a10c-c3b080a431bf/LabelUrls/")
                            .post(formBody)
                            .addHeader("Authorization", Credentials.basic("yGap9gh3cw-lk3NfMsR5ybUBqWyK7KcD", ""))
                            .build();

                    response= client.newCall(request).execute();
                }
                catch (Exception e)
                {
                    return "No response from server"+e;

//                    Toast.makeText(getApplicationContext(), "Eception e"+e, Toast.LENGTH_SHORT).show();
                }
//

                try {
                    return response.body().string();
                } catch (IOException e) {
                    return "No response from server";
                }


            }
        }
        GetJSON getJSON = new GetJSON();
        getJSON.execute();
    }
    void loadInTF(String s)
    {


        imageView.setImageBitmap(bitmap);
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        if(s.contains("unbloom"))
        {

            t1.setText("Disease  detected");
        }
        else
        {
            t1.setText("Disease not detected");
        }

    }
}
