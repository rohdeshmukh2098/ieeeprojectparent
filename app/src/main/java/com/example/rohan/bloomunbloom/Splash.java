package com.example.rohan.bloomunbloom;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
//        getSupportActionBar().hide();
        Thread thread = new Thread() {
            public void run() {
                try {
                    sleep( 3700);
                    startActivity(new Intent(Splash.this,UserHome.class));
                    finish();
                } catch (Exception ex) {

                }
            }
        };
        thread.start();
    }
}
